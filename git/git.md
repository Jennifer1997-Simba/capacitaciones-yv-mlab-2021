# Menu 
- [¿Que es git?](#¿queesgit?)
- [Comandos de git en consola](#comandosdegitenconsola)
- [Clientes de git ](#clientesdegit)
- [Clonacion de proyecto por consola y por cliente](#clonaciondeproyectoporconsolaypocliente)
- [Commit por consola y por cliente Craken ](#commit)
- [Ramas desde Kraken](#ramas)
- [Marge](#marge)


# ¿Que es git?

Un sistema de control de versiones nos va a servir para trabajar en equipo de una manera mucho más simple y optima cuando estamos desarrollando software.
El git nos ayuda a poder realizar comandos en nuestra terminal 

 # Comandos de git en consola

 ## - git init
  Este comando nos sirve para para crear nuestra carpeta .git y asu crear nuestro repositorio
  <center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-init.PNG" width="500">


## - git add.
Este comando nos sirve para agragar todos los cambios en nuestra master
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-add.PNG" width="500">

## - git clone 
Este comando nos sirve para clonar archivos
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-clone.PNG" width="500">

## - git commit

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-commit.PNG" width="500">

## - git push
Este comando nos sirve para subir los cambios en el repositorio
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-push.PNG" width="500">

## - git touch
Este comando nos sirve para para crear nuestra carpeta .git y asu crear nuestro repositorio
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-touch.PNG" width="500">

## - git pull
Este comando nos sirve para fusionar una rama con otra rama activa.
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git-pull.PNG" width="500">

## - git status
Este comando nos sirve para indicar el estado del repositorio
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git.status.PNG" width="500">

<br>
<br>
<br>

# Clientes de git 
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clientesgit.PNG" width="500">


  
  # Clonacion de proyecto por consola y por cliente
  <center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonacion.PNG" width="500">
   <center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonercliente.PNG" width="500">

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitcloneconsola.PNG" width="500">



  # Commit por consola y por cliente Craken
  <center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commitencliente.PNG" width="500">

Commit  por consola

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git.status.PNG" width="500">



  # Ramas desde Kraken
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ramas.PNG" width="500">

# Marge
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ramas.PNG" width="500">