# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)

# Informacion de como crear
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitacceder.PNG" width="500">
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ingresogit.PNG" width="500">

# Crear repositorios
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/repositorio.PNG" width="500">
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/repositorio1.PNG" width="500">

# Crear grupos
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/crear%20grupo.PNG" width="500">

# Crear issues
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/iusees.PNG" width="500">


# Crear labels

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/labels.PNG" width="500">

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/labels1.PNG" width="500">

# Roles que cumplen
Los roles que cumplen son organizar bien cada uno de los repositorios que estemos realixando 

# Agregar miembros
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/mienbros.PNG" width="500">

# Crear borads y manejo de boards
<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/board.PNG" width="500">

<center><img src="https://gitlab.com/Jennifer1997-Simba/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/crear.PNG" width="500">